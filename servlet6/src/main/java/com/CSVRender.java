package com;

import java.io.IOException;

import seminar.CsvStatement;
import seminar.SeminarRepository;

public class CSVRender implements ServletRender {

	@Override
	public void renderOn(Context context) throws IOException {
		context.response().setContentType("text/csv");
		context.response().setHeader("Content-Disposition", "attachment; filename=name.csv");
		context.response().getWriter().write(new CsvStatement().details(new SeminarRepository().first()));		
	}

}
