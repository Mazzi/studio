package com;

import java.io.IOException;

import seminar.HtmlStatement;
import seminar.SeminarRepository;

public class HtmlRender implements ServletRender {

	@Override
	public void renderOn(Context context) throws IOException {
		context.response().setContentType("text/html");
		context.response().getWriter().write(new HtmlStatement().details(new SeminarRepository().first()));
	}

}
