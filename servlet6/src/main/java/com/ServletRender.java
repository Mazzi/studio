package com;

import java.io.IOException;

public interface ServletRender {
	void renderOn(Context context) throws IOException;
}
