package seminar;

public class CsvStatement extends Statement {
	@Override
	String headerString(Seminar seminar) {
		return "numero;" + seminar.getName() + ";" + seminar.getDescription() + ";" + seminar.getLocation()
		+ ";" + seminar.getStartingDate() + ";" + seminar.getSeatLeft() + "\n";
	}

	@Override
	String eachStudentString(Student each) {
		return each.getFirstName() + ";" + each.getLastName() + "\n";
	}

	@Override
	String footerString(Seminar seminar) {
		return "";
	}

}
