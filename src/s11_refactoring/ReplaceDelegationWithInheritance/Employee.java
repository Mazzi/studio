package s11_refactoring.ReplaceDelegationWithInheritance;

public class Employee extends Person {
	public String toString() {
		return "Emp: " + getLastName();
	}
}
