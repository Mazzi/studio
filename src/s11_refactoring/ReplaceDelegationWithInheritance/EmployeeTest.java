package s11_refactoring.ReplaceDelegationWithInheritance;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void test() {
		Employee employee = new Employee();
		employee.setName("pippo Pazzo");
		
		assertEquals("pippo Pazzo", employee.getName());
		assertEquals("Emp: Pazzo", employee.toString());
	}

}
