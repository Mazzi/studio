package s11_refactoring.changeBidirectionalAssociationToUnidirectional;

import java.util.HashSet;
import java.util.Set;

public class Customer {
	void addOrder(Order arg) {
		arg.setCustomer(this);
	}

	private Set _orders = new HashSet();

	Set friendOrders() {
		/** should only be used by Order */
		return _orders;
	}

}
