package s11_refactoring.changeBidirectionalAssociationToUnidirectional;

public class Order {
	private Customer _customer;

	Customer getCustomer() {
		return _customer;
	}

	void setCustomer(Customer arg) {
		if (_customer != null) {
			_customer.friendOrders().remove(this);
		}
		
		_customer = arg;
		if (_customer != null) {
			_customer.friendOrders().add(this);
		}
	}

}
