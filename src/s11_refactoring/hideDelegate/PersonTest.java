package s11_refactoring.hideDelegate;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void johnTest() {
		Person manager = new Person();
		Person john = new Person();
		
		john.setDepartment(new Department(manager));
		assertEquals(manager, john.getManager());
	}

}
