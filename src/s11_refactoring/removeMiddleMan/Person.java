package s11_refactoring.removeMiddleMan;

public class Person {
	Department _department;

	public void setDepartment(Department department) {
		_department = department;
	}
	
	public Department getDepartment() {
		return _department;
	}
}
