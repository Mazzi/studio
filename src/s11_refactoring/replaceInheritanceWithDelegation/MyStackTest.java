package s11_refactoring.replaceInheritanceWithDelegation;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyStackTest {

	@Test
	public void pop() {
		MyStack stack = new MyStack();
		stack.push("Gatto");
		assertEquals("Gatto", stack.pop());
	}

	@Test
	public void size() throws Exception {
		MyStack stack = new MyStack();
		stack.push("Gatto");

		assertEquals(1, stack.size());
	}
	
	@Test
	public void isEmpty() throws Exception {
		MyStack stack = new MyStack();
		stack.push("Gatto");

		assertEquals(false, stack.isEmpty());
		
	}
}
