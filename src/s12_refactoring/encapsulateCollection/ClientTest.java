package s12_refactoring.encapsulateCollection;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class ClientTest {

	@Test
	public void clientUsage() {
		Person kent = new Person();
		Set s = new HashSet();
		s.add(new Course("Smalltalk Programming", false));
		s.add(new Course("Appreciating Single Malts", true));
		kent.initialize(s);
		assertEquals(2, kent.getCourses().size());
		Course refact = new Course("Refactoring", true);
		kent.addCourse(refact);
		kent.addCourse(new Course("Brutal Sarcasm", false));
		assertEquals(4, kent.getCourses().size());
		kent.removeCourse(refact);
		assertEquals(3, kent.getCourses().size());

		int count = 0;
		for (Course each : kent.getCourses()) {
			if (each.isAdvanced())
				count++;
		}

		System.out.println("count=" + count);
	}

	@Test
	public void testName() throws Exception {

	}

}
