package s12_refactoring.encapsulateCollection;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Person {
	private Set<Course> _courses = new HashSet<Course>();

	public Set<Course> getCourses() {
		return Collections.unmodifiableSet(_courses);
	}

	public void initialize(Set<Course> arg) {
		_courses.clear();
		_courses.addAll(arg);
	}
	
	public void addCourse(Course aCourse) {
		_courses.add(aCourse);
	}
	
	
	public void removeCourse(Course aCourse) {
		_courses.remove(aCourse);
	}
}
