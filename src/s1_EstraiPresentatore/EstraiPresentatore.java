package s1_EstraiPresentatore;

import java.security.SecureRandom;

public class EstraiPresentatore {
	public String estrai(String[] _presentatori) {
		if (_presentatori.length < 1) {
			return "Non mi hai fornito nessun presentatore!";
		}
		return "Il presentatore sar� " + _presentatori[new SecureRandom().nextInt(_presentatori.length)];
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(new EstraiPresentatore().estrai(args));
	}
}
