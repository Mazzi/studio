package s1_EstraiPresentatore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EstraiPresentatoreTest {
	@Test
	public void noPresenter() {
		assertEquals("Non mi hai fornito nessun presentatore!", new EstraiPresentatore().estrai(new String[]{}));
	}
	
	@Test
	public void onePresenter() throws Exception {
		assertEquals("Il presentatore sar� Chicco", new EstraiPresentatore().estrai(new String[]{"Chicco"}));
	}
	
	@Test
	public void oneOfTheProvidedPresenter() throws Exception {
		String selected = new EstraiPresentatore().estrai(new String[]{"Chicco", "Mattia"});
		assertTrue(selected.equals("Il presentatore sar� Chicco") || selected.equals("Il presentatore sar� Mattia"));
	}
	

}
