package s2_Seminar;

import java.util.Arrays;
import java.util.List;

public class Seminar {
	private Course _course;
	private String _location;
	private List<Enrollment> _enrollments;

	public Seminar(Course course, String location, Enrollment... enrollments) {
		_course = course;
		_location = location;
		_enrollments = Arrays.asList(enrollments);
		
	}
	public String getName() {
		return _course.getName() + "(max: " + _course.getNumber() + ")";
	}

	public String getDescription() {
		return _course.getDescription();
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatLeft() {
		return _course.getNumber() - _enrollments.size();
	}

	public String getStudentList() {
		String studentList = "[\n";
		for (Enrollment each : _enrollments) {
			studentList += each.getInfo() + "\n";
		}
		return studentList + "]";
	}

}
