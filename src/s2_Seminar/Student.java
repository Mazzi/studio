package s2_Seminar;

public class Student {
	private String _fullName;
	
	public Student(String fullName) {
		_fullName = fullName;
	}

	public String getFullName() {
		return _fullName;
	}

	public String getInfo() {
		return getFullName();
	}

}
