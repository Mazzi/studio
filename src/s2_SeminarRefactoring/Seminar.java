package s2_SeminarRefactoring;

import java.util.Arrays;
import java.util.List;

public class Seminar {
	private String _location;
	private List<String> _students;
	private String _name;
	private int _number;
	private String _description;

	public Seminar(String name, int number, String description, String location, String[] students) {
		_name = name;
		_number = number;
		_description = description;
		_location = location;
		_students = Arrays.asList(students);
		
	}
	public String getName() {
		return _name + "(max: " + _number + ")";
	}

	public String getDescription() {
		return _description;
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatLeft() {
		return _number - _students.size();
	}

	public String getStudentList() {
		if (_students.isEmpty()) {
			return "[]";
		}
		
		String studentList = "[";
		for (String each : _students) {
			studentList += each + ", ";
		}
		return studentList.substring(0, studentList.length() -2) + "]";
	}

}
