package s2_SeminarRefactoring;

public class SeminarDetailsUI {
	private Seminar _seminar;
	
	public SeminarDetailsUI(Seminar seminar) {
		_seminar = seminar;
	}
	
	public static void main(String[] args) {
		System.out.println(new SeminarDetailsUI(
			new Seminar(
				"Hackeraggio Etico", 
				7, 
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new String[] {"Max Ray Butler - Iceman", "Kevin Poulsen - Dark Dante"}
			)				
		).details());
	}

	public String details() {
		return "Nome: " + _seminar.getName() + ", Descrizione: " + _seminar.getDescription() + ", Luogo: " + _seminar.getLocation() 
			+ ", Posti rimanenti: " + _seminar.getSeatLeft() + ", Studenti: " +_seminar.getStudentList();
	}
	
}
