package s2_SeminarRefactoring;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SeminarDetailsUITest {

	@Test
	public void seminarWithZeroEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new String[] {}
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico(max: 7), Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 7, Studenti: []",
			detailsUI.details()
		);
	}
	
	@Test
	public void seminarWithOneEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(	
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new String[] {"Kevin Poulsen - Dark Dante"}
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico(max: 7), Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 6, Studenti: [Kevin Poulsen - Dark Dante]",
			detailsUI.details()
		);
	}
	
	@Test
	public void seminarWithTwoEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new String[] {"Max Ray Butler - Iceman", "Kevin Poulsen - Dark Dante"}
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico(max: 7), Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 5, Studenti: [Max Ray Butler - Iceman, Kevin Poulsen - Dark Dante]",
			detailsUI.details()
		);
		
	}
}
