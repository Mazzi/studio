package s3_SeminarNewRequirement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class SeminarDetailsUI {
	private Seminar _seminar;
	
	public SeminarDetailsUI(Seminar seminar) {
		_seminar = seminar;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		String nomeCorso = "Hackeraggio Etico";
		SeminarDetailsUI seminarDetailsUI = new SeminarDetailsUI(
			new Seminar(
				nomeCorso, 
				7, 
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new Student("Max Ray", "Butler"),
				new Student("Kevin", "Poulsen")
			)				
		);
		
		System.out.println(seminarDetailsUI.details());
		System.out.println(seminarDetailsUI.detailsAsHtml());
		
		PrintWriter writer = new PrintWriter(new File(nomeCorso + ".csv"));
		writer.print(seminarDetailsUI.detailsAsCsv());
		writer.close();
		
	}

	public String details() {
		String output = "Nome: " + _seminar.getName() + ", Descrizione: " + _seminar.getDescription() + ", Luogo: " + _seminar.getLocation() 
					+ ", Posti rimanenti: " + _seminar.getSeatLeft() + ", Studenti: [";
		
		List<Student> studentList = _seminar.getStudentList();
		if (studentList.isEmpty()) {
			return output + "]";
		}
		
		for (Student each : studentList) {
				output += each.getFirstName() + " " + each.getLastName() + ", ";
		}
		
		return output.substring(0, output.length() -2) + "]";
	}

	public String detailsAsHtml() {
		String html = "<html>" + 
		 "<head>" + 
		 "     <title>" + _seminar.getName() + "</title>" + 
		 "</head>" + 
		 "<body>" + 
		 "    <div>" + _seminar.getName() + ":</div>" + 
		 "    <ul>" + 
		 "          <li>" + _seminar.getDescription() + "</li>" + 
		 "          <li>" + _seminar.getLocation() + "</li>" + 
		 "          <li>" + _seminar.getSeatLeft() + "</li>" + 
		 "    </ul>" + 
		 "    <div>partecipanti:</div>" + 
		 "    <ul>";
		
		for (Student each : _seminar.getStudentList()) {
			html += "          <li>" + each.getFirstName() + " " + each.getLastName() + "</li>"; 
		}
		
		html += "    </ul>" + 
		 "</body>" + 
		 "</html>";
		
		return html;
	}

	public String detailsAsCsv() {
		String csv = "numero;"+_seminar.getName()+";"+_seminar.getDescription()+";"+_seminar.getLocation()+";"+_seminar.getSeatLeft()+"\n";
		
		for (Student each : _seminar.getStudentList()) {
			csv += each.getFirstName() + ";" + each.getLastName() + "\n";
		}
		
		return csv;
	}
	
}
