package s3_SeminarNewRequirement;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SeminarDetailsUITest {

	@Test
	public void seminarWithZeroEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio"
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 7, Studenti: []",
			detailsUI.details()
		);
	}
	
	@Test
	public void seminarWithOneEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(	
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new Student("Kevin", "Poulsen - Dark Dante")
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 6, Studenti: [Kevin Poulsen - Dark Dante]",
			detailsUI.details()
		);
	}
	
	@Test
	public void seminarWithTwoEnrollment() {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
			new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new Student("Max", "Ray Butler - Iceman"),
				new Student("Kevin", "Poulsen - Dark Dante")
			)				
		);
		
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 5, Studenti: [Max Ray Butler - Iceman, Kevin Poulsen - Dark Dante]",
			detailsUI.details()
		);
		
	}
	
	@Test
	public void seminarAsHtml() throws Exception {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
		new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new Student("Max", "Ray Butler"),
				new Student("Kevin", "Poulsen")
			)				
		);
		
		String expectedHtml = "<html>" + 
				 "<head>" + 
				 "     <title>Hackeraggio Etico</title>" + 
				 "</head>" + 
				 "<body>" + 
				 "    <div>Hackeraggio Etico:</div>" + 
				 "    <ul>" + 
				 "          <li>Violare un sistema informatico non presume sempre cattive intenzioni</li>" + 
				 "          <li>Manno - stabile Suglio</li>" + 
				 "          <li>5</li>" + 
				 "    </ul>" + 
				 "    <div>partecipanti:</div>" + 
				 "    <ul>" + 
				 "          <li>Max Ray Butler</li>" + 
				 "          <li>Kevin Poulsen</li>" + 
				 "    </ul>" + 
				 "</body>" + 
				 "</html>";
		
		assertEquals(
			expectedHtml,
			detailsUI.detailsAsHtml()
		);
	}
	
	@Test
	public void seminarAsCsv() throws Exception {
		SeminarDetailsUI detailsUI = new SeminarDetailsUI(
		new Seminar(
				"Hackeraggio Etico",
				7,
				"Violare un sistema informatico non presume sempre cattive intenzioni",
				"Manno - stabile Suglio",
				new Student("Max Ray", "Butler"),
				new Student("Kevin", "Poulsen")
			)				
		);
		
		String expectedCsv = "numero;Hackeraggio Etico;Violare un sistema informatico non presume sempre cattive intenzioni;Manno - stabile Suglio;5\n" +
				"Max Ray;Butler\n" +
				"Kevin;Poulsen\n";
		
		assertEquals(
			expectedCsv,
			detailsUI.detailsAsCsv()
		);		
	}
}
