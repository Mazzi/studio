package s3_SeminarNewRequirement;

public class Student {
	private String _firstName;
	private String _lastName;
	
	public Student(String firstName, String lastName) {
		_firstName = firstName;
		_lastName = lastName;
	}

	public String getFirstName() {
		return _firstName;
	}

	public String getLastName() {
		return _lastName;
	}

}
