package s4_SeminarTemplateMethod;

public class HtmlStatement extends Statement {

	@Override
	String headerString(Seminar seminar) {
		return "<html>" + "<head>" + "     <title>" + seminar.getName() + "</title>" + "</head>" + "<body>"
				+ "    <div>" + seminar.getName() + ":</div>" + "    <ul>" + "          <li>" + seminar.getDescription()
				+ "</li>" + "          <li>" + seminar.getLocation() + "</li>" + "          <li>"
				+ seminar.getSeatLeft() + "</li>" + "    </ul>" + "    <div>partecipanti:</div>" + "    <ul>";
	}

	@Override
	String eachStudentString(Student each) {
		return "          <li>" + each.getFirstName() + " " + each.getLastName() + "</li>";
	}

	@Override
	String footerString(Seminar seminar) {
		return "    </ul>" + "</body>" + "</html>";
	}

}
