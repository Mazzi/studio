package s4_SeminarTemplateMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seminar {
	private String _location;
	private List<Student> _students;
	private String _name;
	private int _number;
	private String _description;

	public Seminar(String name, int number, String description, String location, Student... students) {
		_name = name;
		_number = number;
		_description = description;
		_location = location;
		_students = Arrays.asList(students);
		
	}
	public String getName() {
		return _name ;
	}

	public String getDescription() {
		return _description;
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatLeft() {
		return _number - _students.size();
	}

	public List<Student> getStudentList() {
		return new ArrayList<Student>(_students);
	}
}
