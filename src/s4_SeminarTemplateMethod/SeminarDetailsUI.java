package s4_SeminarTemplateMethod;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class SeminarDetailsUI {
	public static void main(String[] args) throws FileNotFoundException {
		String nomeCorso = "Hackeraggio Etico";
		
		Seminar seminar = new Seminar(
			nomeCorso, 
			7, 
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio",
			new Student("Max Ray", "Butler"),
			new Student("Kevin", "Poulsen")
		);
		
		
		System.out.println(new GrezzaStatement().details(seminar));
		System.out.println(new HtmlStatement().details(seminar));
		
		PrintWriter writer = new PrintWriter(new File(nomeCorso + ".csv"));
		writer.print(new CsvStatement().details(seminar));
		writer.close();
	}
	
}
