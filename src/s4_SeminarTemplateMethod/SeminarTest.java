package s4_SeminarTemplateMethod;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SeminarTest {

	@Test
	public void seminarWithZeroEnrollment() {
		Seminar seminar = new Seminar(
			"Hackeraggio Etico",
			7,
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio"
		);		
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 7, Studenti: ",
			new GrezzaStatement().details(seminar)
		);
	}
	
	@Test
	public void seminarWithOneEnrollment() {
		Seminar seminar = new Seminar(	
			"Hackeraggio Etico",
			7,
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio",
			new Student("Kevin", "Poulsen - Dark Dante")
		);
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 6, Studenti: Kevin Poulsen - Dark Dante, ",
			new GrezzaStatement().details(seminar)
		);
	}
	
	@Test
	public void seminarWithTwoEnrollment() {
		Seminar seminar = new Seminar(
			"Hackeraggio Etico",
			7,
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio",
			new Student("Max", "Ray Butler - Iceman"),
			new Student("Kevin", "Poulsen - Dark Dante")
		);				
		
		assertEquals(
			"Nome: Hackeraggio Etico, Descrizione: Violare un sistema informatico non presume sempre cattive intenzioni, "
			+ "Luogo: Manno - stabile Suglio, Posti rimanenti: 5, Studenti: Max Ray Butler - Iceman, Kevin Poulsen - Dark Dante, ",
			new GrezzaStatement().details(seminar)
		);
		
	}
	
	@Test
	public void seminarAsHtml() throws Exception {
		Seminar seminar = new Seminar(
			"Hackeraggio Etico",
			7,
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio",
			new Student("Max", "Ray Butler"),
			new Student("Kevin", "Poulsen")
		);
		
		String expectedHtml = "<html>" + 
				 "<head>" + 
				 "     <title>Hackeraggio Etico</title>" + 
				 "</head>" + 
				 "<body>" + 
				 "    <div>Hackeraggio Etico:</div>" + 
				 "    <ul>" + 
				 "          <li>Violare un sistema informatico non presume sempre cattive intenzioni</li>" + 
				 "          <li>Manno - stabile Suglio</li>" + 
				 "          <li>5</li>" + 
				 "    </ul>" + 
				 "    <div>partecipanti:</div>" + 
				 "    <ul>" + 
				 "          <li>Max Ray Butler</li>" + 
				 "          <li>Kevin Poulsen</li>" + 
				 "    </ul>" + 
				 "</body>" + 
				 "</html>";
		
		assertEquals(
			expectedHtml,
			new HtmlStatement().details(seminar)
		);
	}
	
	@Test
	public void seminarAsCsv() throws Exception {
		Seminar seminar = new Seminar(
			"Hackeraggio Etico",
			7,
			"Violare un sistema informatico non presume sempre cattive intenzioni",
			"Manno - stabile Suglio",
			new Student("Max Ray", "Butler"),
			new Student("Kevin", "Poulsen")
		);				
		
		String expectedCsv = "numero;Hackeraggio Etico;Violare un sistema informatico non presume sempre cattive intenzioni;Manno - stabile Suglio;5\n" +
				"Max Ray;Butler\n" +
				"Kevin;Poulsen\n";
		
		assertEquals(
			expectedCsv,
			new CsvStatement().details(seminar)
		);		
	}
}
