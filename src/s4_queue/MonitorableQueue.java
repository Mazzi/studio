package s4_queue;

public class MonitorableQueue implements Queue {
	private SimpleQueue _queue;
	private int _size = 0;

	public MonitorableQueue() {
		_queue = new SimpleQueue();
	}
	
	public boolean enqueue(String data) {
		_size++;
		return _queue.enqueue(data);
	}

	public String dequeue() {
		_size--;
		return _queue.dequeue();
	}
	public int size() {
		return _size;
	}

	public String peek() {
		String peeked = _queue.dequeue();
		
		// E adesso vai di ricostruzione della sequenza originale! uao!!!
		SimpleQueue newQueue = new SimpleQueue();
		newQueue.enqueue(peeked);
		String data = null;
		while ((data = _queue.dequeue()) != null) {
			newQueue.enqueue(data);
		}
		_queue = newQueue;
		
		return peeked;
	}

	public boolean isFull() {
		return false;
	}
	
}
