package s4_queue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MonitorableQueueTest {
	
	@Test
	public void behavesLikeSimpleQueue() throws Exception {
		MonitorableQueue queue = new MonitorableQueue();
		queue.enqueue("primo");
		queue.enqueue("secondo");
		
		assertEquals("primo", queue.dequeue());
		assertEquals("secondo", queue.dequeue());
		assertEquals(null, queue.dequeue());
	}

	@Test
	public void size_no_element() throws Exception {
		MonitorableQueue queue = new MonitorableQueue();
		assertEquals(0, queue.size());
	}
	
	@Test
	public void size_two_element() throws Exception {
		MonitorableQueue queue = new MonitorableQueue();
		queue.enqueue("primo");
		queue.enqueue("secondo");
		
		assertEquals(2, queue.size());
	}
	
	@Test
	public void peek() throws Exception {
		MonitorableQueue queue = new MonitorableQueue();
		queue.enqueue("primo");
		
		assertEquals("primo", queue.peek());
		assertEquals("primo", queue.peek());
	}

	@Test
	public void isFull() throws Exception {
		MonitorableQueue queue = new MonitorableQueue();
		assertEquals(false, queue.isFull());

		queue.enqueue("primo");
		assertEquals(false, queue.isFull());
		
	}
}
