package s4_queue;

public interface Queue {
	public boolean enqueue(String data);
	public String dequeue();
}
