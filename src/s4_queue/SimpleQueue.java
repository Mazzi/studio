package s4_queue;

import java.util.ArrayList;
import java.util.List;

public class SimpleQueue implements Queue {
	List<String> _queue = new ArrayList<String>();
	
	public boolean enqueue(String data) {
		return _queue.add(data);
	}

	public String dequeue() {
		return _queue.isEmpty() ? null : _queue.remove(0);
	}

}
