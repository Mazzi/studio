package s4_queue;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SimpleQueueTest {

	@Test
	public void oneElement() {
		SimpleQueue queue = new SimpleQueue();
		queue.enqueue("primo");
		assertEquals("primo", queue.dequeue());
		assertEquals(null, queue.dequeue());
	}
	
	@Test
	public void twoElements() throws Exception {
		SimpleQueue queue = new SimpleQueue();
		queue.enqueue("primo");
		queue.enqueue("secondo");
		
		assertEquals("primo", queue.dequeue());
		assertEquals("secondo", queue.dequeue());
		assertEquals(null, queue.dequeue());
		
	}

}
