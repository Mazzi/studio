package s5_EstraiPresentatoreNewRequirement;

import java.io.File;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class EstraiPresentatore {
	public String estrai(List<String> esclusi) {
		List<String> presentatori = new ArrayList<String>(Arrays.asList(new String[] {"mattia", "chicco", "stefano", "valentino", "dario", "gennaro", 
				"manlio", "enrico", "vacca", "alessandro", "pino", "franco", "matteo", "massi", "gabri" }));

		presentatori.removeAll(esclusi);
		
		return presentatori.get(new SecureRandom().nextInt(presentatori.size()));
	}
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String now = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		List<String> esclusi = Arrays.asList(args);
		System.out.println("Il presentatore di oggi (" + now + ") sar�: " + new EstraiPresentatore().estrai(esclusi));
		
		PrintWriter log = new PrintWriter(new File(now + "_esclusi.log"));
		for (String each : esclusi) {
			log.println(each);
		}
		log.close();
		
		
	}
}
