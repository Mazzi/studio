package s5_SeminarNewRequirement;

public class GrezzaStatement extends Statement {
	@Override
	String headerString(Seminar seminar) {
		return "Nome: " + seminar.getName() + ", Descrizione: " + seminar.getDescription() + ", Luogo: "
				+ seminar.getLocation() + ", Data inizio: " + seminar.getStartingDate() + ", Posti rimanenti: " + seminar.getSeatLeft() + ", Studenti: ";
	}

	@Override
	String eachStudentString(Student each) {
		return each.getFirstName() + " " + each.getLastName() + ", ";
	}

	@Override
	String footerString(Seminar seminar) {
		return "";
	}

}
