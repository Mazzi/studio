package s5_SeminarNewRequirement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seminar {
	private String _location;
	private List<Student> _students;
	private String _name;
	private int _number;
	private String _description;
	private String _startingDate;

	public Seminar(String name, int number, String description, String location, String startingDate, Student... students) {
		_name = name;
		_number = number;
		_description = description;
		_location = location;
		_startingDate = startingDate;
		_students = Arrays.asList(students);
		
	}
	public String getName() {
		return _name ;
	}

	public String getDescription() {
		return _description;
	}

	public String getLocation() {
		return _location;
	}

	public int getSeatLeft() {
		return _number - _students.size();
	}

	public List<Student> getStudentList() {
		return new ArrayList<Student>(_students);
	}
	public String getStartingDate() {
		return _startingDate;
	}
}
