package s5_SeminarNewRequirement;

public abstract class Statement {
	public String details(Seminar seminar) {
		
		StringBuffer result = new StringBuffer();
		result.append(headerString(seminar));
		
		for (Student each : seminar.getStudentList()) {
			result.append(eachStudentString(each));
		}
		
		result.append(footerString(seminar));
		return result.toString();
	}

	abstract String headerString(Seminar seminar);
	abstract String eachStudentString(Student each);
	abstract String footerString(Seminar seminar);
}
