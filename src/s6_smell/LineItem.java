package s6_smell;

import java.io.PrintWriter;

 
class LineItem {
    protected int _productId;
    private int _imageID;
    private int _qty;
    private int _unitprice;
 
    public LineItem(int prodID, int ImageID, int inQty, int unitPrice) {
        _productId = prodID;
        _imageID = ImageID;
        _qty = inQty;
        _unitprice = unitPrice;
    }
 
	int calculateItemTotal() {
		return _unitprice * _qty;
	}

	void printOn(PrintWriter pw) {
		pw.println("Begin Line Item");
		pw.println("Product = " + _productId);
		pw.println("Image = " + _imageID);
		pw.println("Quantity = " + _qty);
		pw.println("Total = " + calculateItemTotal());
		pw.println("End Line Item");
	}

}
