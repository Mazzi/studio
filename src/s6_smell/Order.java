package s6_smell;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
 
public class Order {
 
    private List<LineItem> _lineItemList;
 
    public Order(List<LineItem> lineItemList) {
        _lineItemList = lineItemList;
    }
 
    // writes this order object to the specified print writer
    public void writeOrder(Order order, PrintWriter pw) {
    	for (LineItem item : _lineItemList) {
    		item.printOn(pw);
		}
        pw.println("Order total = " + getTotal());
    }

	public int getTotal() {
		int total = 0;
		for (LineItem item : _lineItemList) {
			total += item.calculateItemTotal();
		}
        return total;
    }
 
    /** This method saves the order to the database */
    public void saveOrder()  throws SQLException {
    	String sql = createSqlStatement();
        PreparedStatement orderStatement = null;
        orderStatement = setConnection().prepareStatement(sql);
        //set all parameters
       
        //execute statement
        orderStatement.executeUpdate();
    }

	String createSqlStatement() {
		return "INSERT INTO T_ORDER (AUTHORIZATION_CODE, SHIPMETHOD_ID, USER_ID, ADDRESS_ID) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	private Connection setConnection() {
		return null;
	}
}
