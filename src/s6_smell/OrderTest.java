package s6_smell;

import static org.junit.Assert.assertEquals;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.junit.Test;

public class OrderTest {

	@Test
	public void getTotal() {
		assertEquals(buildAFinctionalOrder().getTotal(), 93);
	}	
	
	@Test
	public void writeOrder() throws Exception {
		StringWriter result = new StringWriter();
		Order order = buildAFinctionalOrder();
		order.writeOrder(order, new PrintWriter(result));

		String expected = "Begin Line Item\nProduct = 1\nImage = 200\nQuantity = 2\nTotal = 18\nEnd Line Item\nBegin Line Item\nProduct = 2\nImage = 300\nQuantity = 5\nTotal = 75\nEnd Line Item\nOrder total = 93\n";
		assertEquals(expected, result.toString());
		
	}
	
	@Test
	public void createSqlStatement() throws Exception {
		assertEquals("INSERT INTO T_ORDER (AUTHORIZATION_CODE, SHIPMETHOD_ID, USER_ID, ADDRESS_ID) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", buildAFinctionalOrder().createSqlStatement());
	}
	
	private Order buildAFinctionalOrder() {
		ArrayList<LineItem> items = new ArrayList<LineItem>();
		items.add(new LineItem(1, 200, 2, 9));
		items.add(new LineItem(2, 300, 5, 15));
		
		return new Order(items);
	}
	
	
}
