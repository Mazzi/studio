package s7_refactoring.ReplaceTempWithQuery;

public class Price {
	private int _quantity;
	private double _itemPrice;
	
	public Price(int quantity, double itemPrice) {
		_quantity = quantity;
		_itemPrice = itemPrice;
	}

	public double calculate() {
		return basePrice() > 1000 ? basePrice() * 0.95 : basePrice() * 0.98;
	}

	private double basePrice() {
		return _quantity * _itemPrice;
	}

}
