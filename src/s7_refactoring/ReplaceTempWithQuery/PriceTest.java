package s7_refactoring.ReplaceTempWithQuery;

import static org.junit.Assert.*;

import org.junit.Test;

public class PriceTest {

	@Test
	public void test() {
		assertEquals(1899.81, new Price(2, 999.9).calculate(), 0);
		assertEquals(1900.19, new Price(2, 1000.1).calculate(), 0);
	}

}
