package s7_refactoring.SubstituteAlgorith;

import java.util.Arrays;
import java.util.List;

public class PersonUtil {
	 String oldFoundPerson(String[] people) {
		 for (int i = 0; i < people.length; i++) {
			 if (people[i].equals ("Don")){
				 return "Don";
			 }
			 
			 if (people[i].equals ("John")){
				 return "John";
			 }
			 
			 if (people[i].equals ("Kent")){
				 return "Kent";
			 }
		 }
		 
		 return "";
	 }
	 
	 String foundPerson(String[] people) {
		 List<String> candidates = Arrays.asList(new String[] {"Don", "John", "Kent"});
		 for (String each : Arrays.asList(people)) {
			 if (candidates.contains(each)) {
				 return each;
			 }
		 }
		return "";
	 }
		
	

}
