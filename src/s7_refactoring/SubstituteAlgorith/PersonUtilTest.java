package s7_refactoring.SubstituteAlgorith;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonUtilTest extends PersonUtil {

	@Test
	public void test() {
		assertEquals("Don", new PersonUtil().foundPerson(new String[] { "gianni", "Don"}));
		assertEquals("John", new PersonUtil().foundPerson(new String[] { "gianni", "John"}));
		assertEquals("Kent", new PersonUtil().foundPerson(new String[] { "gianni", "Kent"}));
		assertEquals("Don", new PersonUtil().foundPerson(new String[] { "Don", "Kent"}));
	}

}
