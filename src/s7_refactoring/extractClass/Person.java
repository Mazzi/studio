package s7_refactoring.extractClass;

public class Person {
	private String _name;
	private TelephoneNumber _telephoneNumber = new TelephoneNumber();
	
	public String getName() {
		return _name;
	}

	public String getTelephoneNumber() {
		return _telephoneNumber.getTelephoneNumber();
	}
	
	public TelephoneNumber getOfficeTelephone() {
		return _telephoneNumber;
	}

}
