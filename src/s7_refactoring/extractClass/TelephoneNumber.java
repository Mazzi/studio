 package s7_refactoring.extractClass;

public class TelephoneNumber {
	private String _officeAreaCode;
	private String _officeNumber;
	
	public void setAreaCode(String arg) {
		_officeAreaCode = arg;
	}

	public void setOfficeNumber(String arg) {
		_officeNumber = arg;
	}

	public String getTelephoneNumber() {
		return ("(" + _officeAreaCode + ") " + _officeNumber);
	}
	
}
