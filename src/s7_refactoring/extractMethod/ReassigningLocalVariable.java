package s7_refactoring.extractMethod;

import java.util.Enumeration;
import java.util.Vector;

public class ReassigningLocalVariable {
	private Vector<Order> _orders;
	private String _name = "name";

	void printOwing() {
		 Enumeration<Order> e = _orders.elements();
		 double outstanding = 0.0;
		 printBanner();
		 // calculate outstanding
		 while (e.hasMoreElements()) {
			 Order each = (Order) e.nextElement();
			 outstanding += each.getAmount();
		 }
		 
		 printDetails(outstanding);
	 }

	private void printDetails(Double outstanding) {
		//print details
		System.out.println ("name:" + _name);
		System.out.println ("amount" + outstanding);
	}

	private void printBanner() {
		System.out.println ("**************************");
		System.out.println ("***** Customer Owes ******");
		System.out.println ("**************************");
	}
}
