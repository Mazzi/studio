package s7_refactoring.extractMethod;

import java.util.Enumeration;
import java.util.Vector;

public class UsingLocalVariables {
	private Vector<Order> _orders;
	private String _name = "name";

	void printOwing() {
		 printBanner();
		 double outstanding = getOutStanding();
		 printDetails(outstanding);
	 }

	private double getOutStanding() {
		double result = 0.0;
		Enumeration<Order> e = _orders.elements();
		while (e.hasMoreElements()) {
			Order each = (Order) e.nextElement();
			result += each.getAmount();
		}
		return result;
	}

	private void printDetails(Double outstanding) {
		System.out.println ("name:" + _name);
		System.out.println ("amount" + outstanding);
	}

	private void printBanner() {
		System.out.println ("**************************");
		System.out.println ("***** Customer Owes ******");
		System.out.println ("**************************");
	}
}
