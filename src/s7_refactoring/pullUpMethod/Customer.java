package s7_refactoring.pullUpMethod;

import java.util.Date;

public abstract class Customer {
	protected Date lastBillDate;
	
	public abstract double chargeFor(Date lastBillDate2, Date date);

	public void createBill(Date date) {
		double chargeAmount = chargeFor(lastBillDate, date);
		addBill (date, chargeAmount);
	}

	public void addBill(Date dat, Double amount) {
		
	}
}
