package s7_refactoring.pullUpMethod;

import java.util.Date;

public class PreferredCustomer extends Customer {
	public double chargeFor(Date start, Date end) {
		return 1.4;
	}
	
}
