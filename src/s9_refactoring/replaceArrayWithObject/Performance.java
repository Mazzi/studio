package s9_refactoring.replaceArrayWithObject;

public class Performance {

	private String _name;
	private int _wins;

	public Performance(String name, String wins) {
		_name = name;
		_wins = Integer.parseInt(wins);
	}

	public String name() {
		return _name;
	}

	public int wins() {
		return _wins;
	}

}
