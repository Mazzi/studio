package s9_refactoring.replaceArrayWithObject;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TeamTest {

	@Test
	public void statistic() {
	    Performance performance = new Performance("Liverpool", "15");
	    
	    assertEquals("Liverpool", performance.name());
	    assertEquals(15, performance.wins());
	}

}
