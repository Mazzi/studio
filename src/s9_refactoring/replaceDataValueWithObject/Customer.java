package s9_refactoring.replaceDataValueWithObject;

public class Customer {
	private String _name;
	
	public Customer(String name) {
		_name = name;
	}

	public String getName() {
		return _name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Customer) {
			return _name.equals(((Customer)obj)._name);
		}
		return false;
	}
	
}
