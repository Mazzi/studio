package s9_refactoring.replaceDataValueWithObject;

import java.util.List;

public class Order {
	private Customer _customer;

	public Order(String customerName) {
		_customer = new Customer(customerName);
	}

	public Customer getCustomer() {
		return _customer;
	}

	public void setCustomer(Customer customer) {
		_customer = customer;
	}

	public static int numberOfOrdersFor(List<Order> orders, String customer) {
		int result = 0;
		for (Order order : orders) {
			if (order.getCustomer().equals(new Customer(customer)))
				result++;
		}
		return result;
	}

}
