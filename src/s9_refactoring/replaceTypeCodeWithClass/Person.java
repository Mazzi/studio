package s9_refactoring.replaceTypeCodeWithClass;

public class Person {
	private BloodGroup _group;

	public Person(BloodGroup bloodGroup) {
		_group = bloodGroup;
	}

	public void setBloodGroup(BloodGroup group) {
		_group = group;
	}

	public BloodGroup getBloodGroup() {
		return _group;
	}
}
