package s9_refactoring.replaceTypeCodeWithClass;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void test() {
		Person person = new Person(BloodGroup.AB);
		assertEquals(BloodGroup.AB, person.getBloodGroup());
	}

}
