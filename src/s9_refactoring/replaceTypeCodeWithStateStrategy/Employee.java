package s9_refactoring.replaceTypeCodeWithStateStrategy;

public class Employee {
	private final Type _type;

	Employee(Type type) {
		_type = type;
	}

	int payAmount() {
		return _type.payAmount();
	}

}
