package s9_refactoring.replaceTypeCodeWithStateStrategy;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void managerSalary() {
		Employee employee = new Employee(new Manager());
		assertEquals(2500, employee.payAmount());
	}

}
