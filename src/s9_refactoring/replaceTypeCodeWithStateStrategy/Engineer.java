package s9_refactoring.replaceTypeCodeWithStateStrategy;

public class Engineer extends Type {

	@Override
	int payAmount() {
		return _monthlySalary;
	}

}
