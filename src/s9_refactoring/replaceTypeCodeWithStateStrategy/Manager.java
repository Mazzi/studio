package s9_refactoring.replaceTypeCodeWithStateStrategy;

public class Manager extends Type {

	@Override
	int payAmount() {
		return _monthlySalary + _bonus;
	}

}
