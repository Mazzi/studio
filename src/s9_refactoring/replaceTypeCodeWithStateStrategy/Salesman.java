package s9_refactoring.replaceTypeCodeWithStateStrategy;

public class Salesman extends Type {

	@Override
	int payAmount() {
		return _monthlySalary + _commission;
	}

}
