package s9_refactoring.replaceTypeCodeWithStateStrategy;

public abstract class Type {
	protected final int _monthlySalary = 2000;
	protected final int _commission = 100;
	protected final int _bonus = 500;

	abstract int payAmount();
}