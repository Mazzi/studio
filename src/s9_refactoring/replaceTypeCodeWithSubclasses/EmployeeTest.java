package s9_refactoring.replaceTypeCodeWithSubclasses;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void testName() {
		Employee employee = new Manager();
		assertEquals(2, employee.getType());
	}

}
